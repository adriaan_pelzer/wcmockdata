
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path')
  , siteMapper = require ( './siteMapper.js' );

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

console.log(siteMapper.create('/', require('./map.json'), function(dir, data){
	app.get(dir, routes[data]);
}));

app.get('*', function(req, res){
	res.send('<p>error:404</p> <p>the file you are looking for does not exist, has never existed and will never exist, good day sir.</p>', 404);
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
