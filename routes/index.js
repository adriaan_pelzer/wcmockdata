/*
 * GET home page.
 */
endpoints = require("../endpoints.js");

// Globals
exports.leagueTable = function(req, res) {
	res.send({
		"leagueTable" : endpoints.global.leagueTable
	});
};
exports.fixtures = function(req, res) {
	res.send({
		"fixtures" : endpoints.global.fixtures
	});
};

// Id dependent

exports.fixture = function(req, res) {
	res.send({
		"fixture" : endpoints.perId.fixture
	});
};

exports.lineUps = function(req, res) {
	res.send({
		"lineUps" : endpoints.perId.lineUps
	});
};

exports.facts = function(req, res) {
	res.send({
		"facts" : endpoints.perId.facts
	});
};

exports.matchSummary = function(req, res) {
	res.send({
		"matchSummary" : endpoints.perId.matchSummary
	});
};

exports.newsFeed = function(req, res) {
	res.send({
		"newsFeed" : endpoints.perId.newsFeed
	});
};