module.exports.blank = {};

module.exports.global = {
    "leagueTable": {
        "metaData": {
            "lastUpdated": "19/01/2014 17:54"
        },
        "content": [
        {
            "id": 8,
            "teamName": "Chelsea",
            "gamesPlayed": 22,
            "points": 49
        },
        {
            "id": 1,
            "teamName": "Manchester United",
            "gamesPlayed": 22,
            "points": 37
        }
        ]
    },
    "fixtures": {
        "content": [
            {
                "id": 695116,
                "url": "http://generator-test-env-2fnkv2qb6m.elasticbeanstalk.com/norwich-city-vs-hull-city-2014-01-18.html",
                "startTime": "18 Jan - 15:00",
                "competitionName": "English Barclays Premier League",
                "status": "Full Time",
                "active": false,
                "homeTeam": {
                    "id": 45,
                    "score": 1,
                    "shortName": "Norwich",
                    "longName": "Norwich City"
                },
                "awayTeam": {
                    "id": 88,
                    "score": 0,
                    "shortName": "Hull",
                    "longName": "Hull City"
                }
            }
        ]
    }
};

module.exports.perId = {
    "fixture": {
        "content": {
            "id": 695116,
            "url": "http://generator-test-env-2fnkv2qb6m.elasticbeanstalk.com/norwich-city-vs-hull-city-2014-01-18.html",
            "startTime": "18 Jan - 15:00",
            "competitionName": "English Barclays Premier League",
            "status": "Full Time",
            "active": false,
            "homeTeam": {
                "id": 45,
                "score": 1,
                "shortName": "Norwich",
                "longName": "Norwich City"
            },
            "awayTeam": {
                "id": 88,
                "score": 0,
                "shortName": "Hull",
                "longName": "Hull City"
            }
        }
    },
    "newsFeed": {
        "content": [
            {
                "updateId": 1724111,
                "time": "15:03",
                "minute": "2",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "match-update"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": "So close to an instant impact for Nikica Jelavic."
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": "Robert Koren got to the by-line and cut it back to him but the striker missed the target from close range. He hit the outside of the post and but a big chance missed"
                    }
                ]
            },
            {
                "updateId": 1724641,
                "time": "15:15",
                "minute": "14",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "sub"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": " Hull are forced to make an early change and James Chester is replaced by Maynor Figueroa"
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": ""
                    }
                ]
            },
            {
                "updateId": 1725571,
                "time": "15:39",
                "minute": "38",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "yellow-card"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": " Maynor Figueroa shown a yellow card for pulling back Robert Snodgrass"
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": ""
                    }
                ]
            },
            {
                "updateId": 1726111,
                "time": "16:06",
                "minute": "46",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "second-half"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": "Hull get the second half underway at Carrow Road"
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": ""
                    }
                ]
            },
            {
                "updateId": 1727801,
                "time": "16:49",
                "minute": "87",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "goal"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": "GOAL."
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": "Norwich 1-0 Hull, a goal from Ryan Bennett. Robert Snodgrass whipped a corner in and Bennett rose above Alex Bruce to head home"
                    }
                ]
            },
            {
                "updateId": 1727841,
                "time": "16:50",
                "minute": "89",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "red-card"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": " Tom Huddlestone shown a second yellow card for a foul on Leroy Fer and he is sent off "
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": ""
                    }
                ]
            },
            {
                "updateId": 1728031,
                "time": "16:55",
                "minute": "94",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "full-time"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": "FULL-TIME: NORWICH 1-0 HULL "
                },
                "contentTypes": [
                    {
                        "type": "paragraph",
                        "text": ""
                    }
                ]
            },
            {
                "updateId": 6,
                "time": "15:07",
                "minute": "95",
                "source": {
                    "author": {
                        "company": "company-name",
                        "name": "author-name"
                    },
                    "iconSource": "/assets/images/authors/icon-person.png",
                    "type": "statistic"
                },
                "matchTeams": {
                    "home": "Norwich",
                    "away": "Hull"
                },
                "heading": {
                    "text": "Possession"
                },
                "contentTypes": [
                    {
                        "type": "pieChart",
                        "data": [
                            {
                                "labelText": "Norwich",
                                "value": 56.2
                            },
                            {
                                "labelText": "Hull",
                                "value": 43.8
                            }
                        ]
                    }
                ]
            }
        ]
    },
    "matchSummary": {
        "metaData": {
            "attendance": 26655,
            "kickOff": "18.01.14 15:00",
            "referee": "Howard Webb",
            "location": "Carrow Road"
        },
        "content": {
            "homeTeam": {
                "name": "Norwich City",
                "goalsScored": [
                    {
                        "minuteOfGoal": 87,
                        "name": "Ryan Bennett"
                    }
                ]
            },
            "awayTeam": {
                "name": "Hull City",
                "goalsScored": []
            }
        }
    },
    "inSummary": {
        "content": [
            {
                "heading": "% Possession",
                "percentages": false,
                "values": {
                    "home": 56.2,
                    "away": 43.8
                }
            },
            {
                "heading": "Total shots",
                "percentages": false,
                "values": {
                    "home": 25,
                    "away": 10
                }
            },
            {
                "heading": "Shots on target",
                "percentages": false,
                "values": {
                    "home": 4,
                    "away": 0
                }
            },
            {
                "heading": "Fouls",
                "percentages": false,
                "values": {
                    "home": 8,
                    "away": 16
                }
            },
            {
                "heading": "Yellow cards",
                "percentages": false,
                "values": {
                    "home": 1,
                    "away": 2
                }
            },
            {
                "heading": "Red cards",
                "percentages": false,
                "values": {
                    "away": 1,
                    "home": 0
                }
            }
        ],
        "key": {
            "homeTeam": "Norwich City",
            "awayTeam": "Hull City"
        }
    },
    "facts": {
        "content": []
    },
    "lineUps": {
        "content": {
            "homeTeam": {
                "players": {
                    "starting": [
                        {
                            "id": 19236,
                            "name": "John Ruddy",
                            "eventUpdate": []
                        },
                        {
                            "id": 28654,
                            "name": "Martin Olsson",
                            "eventUpdate": []
                        },
                        {
                            "id": 42744,
                            "name": "Sebastien Bassong Nguena",
                            "eventUpdate": []
                        },
                        {
                            "id": 19341,
                            "name": "Russell Martin",
                            "eventUpdate": []
                        },
                        {
                            "id": 41727,
                            "name": "Ryan Bennett",
                            "eventUpdate": [
                                "goal-scorer"
                                ]
                        },
                        {
                            "id": 49277,
                            "name": "Leroy Fer",
                            "eventUpdate": []
                        },
                        {
                            "id": 18987,
                            "name": "Robert Snodgrass",
                            "eventUpdate": []
                        },
                        {
                            "id": 19569,
                            "name": "Bradley Johnson",
                            "eventUpdate": [
                                "yellow-card"
                                ]
                        },
                        {
                            "id": 21060,
                            "name": "JonÃƒÂ¡s GutiÃƒÂ©rrez",
                            "eventUpdate": [
                                "substitute-off"
                                ]
                        },
                        {
                            "id": 28221,
                            "name": "Gary Hooper",
                            "eventUpdate": []
                        },
                        {
                            "id": 50165,
                            "name": "Ricky van Wolfswinkel",
                            "eventUpdate": [
                                "substitute-off"
                                ]
                        }
                    ],
                    "substitutions": [
                        {
                            "id": 28499,
                            "name": "Wes Hoolahan",
                            "eventUpdate": []
                        },
                        {
                            "id": 83283,
                            "name": "Nathan Redmond",
                            "eventUpdate": []
                        },
                        {
                            "id": 10954,
                            "name": "Mark Bunn",
                            "eventUpdate": []
                        },
                        {
                            "id": 13164,
                            "name": "Steven Whittaker",
                            "eventUpdate": []
                        },
                        {
                            "id": 18665,
                            "name": "Alexander Tettey",
                            "eventUpdate": []
                        },
                        {
                            "id": 40451,
                            "name": "Anthony Pilkington",
                            "eventUpdate": [
                                "substitute-on"
                                ]
                        },
                        {
                            "id": 19791,
                            "name": "Luciano Becchio",
                            "eventUpdate": [
                                "substitute-on"
                                ]
                        }
                    ]
                },
                "name": "Norwich City"
            },
            "awayTeam": {
                "players": {
                    "starting": [
                        {
                            "id": 12390,
                            "name": "Allan McGregor",
                            "eventUpdate": []
                        },
                        {
                            "id": 19115,
                            "name": "Curtis Davies",
                            "eventUpdate": []
                        },
                        {
                            "id": 19687,
                            "name": "Alex Bruce",
                            "eventUpdate": []
                        },
                        {
                            "id": 43252,
                            "name": "James Chester",
                            "eventUpdate": [
                                "substitute-off"
                                ]
                        },
                        {
                            "id": 37339,
                            "name": "Ahmed Elmohamady",
                            "eventUpdate": []
                        },
                        {
                            "id": 49944,
                            "name": "Jake Livermore",
                            "eventUpdate": []
                        },
                        {
                            "id": 15137,
                            "name": "Liam Rosenior",
                            "eventUpdate": [
                                "yellow-card"
                                ]
                        },
                        {
                            "id": 15109,
                            "name": "Tom Huddlestone",
                            "eventUpdate": [
                                "yellow-card",
                            "red-card"
                                ]
                        },
                        {
                            "id": 27522,
                            "name": "Robert Koren",
                            "eventUpdate": [
                                "substitute-off"
                                ]
                        },
                        {
                            "id": 62419,
                            "name": "Nikica Jelavic",
                            "eventUpdate": []
                        },
                        {
                            "id": 51627,
                            "name": "Yannick Sagbo",
                            "eventUpdate": [
                                "substitute-off"
                                ]
                        }
                    ],
                    "substitutions": [
                        {
                            "id": 20462,
                            "name": "Stephen Quinn",
                            "eventUpdate": []
                        },
                        {
                            "id": 53371,
                            "name": "David Meyler",
                            "eventUpdate": []
                        },
                        {
                            "id": 1881,
                            "name": "Steve Harper",
                            "eventUpdate": []
                        },
                        {
                            "id": 14055,
                            "name": "Abdoulaye Faye",
                            "eventUpdate": []
                        },
                        {
                            "id": 34285,
                            "name": "Maynor Figueroa",
                            "eventUpdate": [
                                "substitute-on",
                            "yellow-card"
                                ]
                        },
                        {
                            "id": 28244,
                            "name": "George Boyd",
                            "eventUpdate": [
                                "substitute-on"
                                ]
                        },
                        {
                            "id": 19182,
                            "name": "Matty Fryatt",
                            "eventUpdate": [
                                "substitute-on"
                                ]
                        }
                    ]
                },
                "name": "Hull City"
            }
        }
    },
    "matchStats": {
        "attack": {
            "content": [
                {
                    "heading": "Goals",
                    "percentages": false,
                    "values": {
                        "home": 1,
                        "away": 0
                    }
                },
                {
                    "heading": "Total shots",
                    "percentages": false,
                    "values": {
                        "home": 25,
                        "away": 10
                    }
                },
                {
                    "heading": "Shots on target",
                    "percentages": false,
                    "values": {
                        "home": 4,
                        "away": 0
                    }
                },
                {
                    "heading": "Blocked Shots",
                    "percentages": false,
                    "values": {
                        "home": 11,
                        "away": 3
                    }
                },
                {
                    "heading": "Shots from outside the box",
                    "percentages": false,
                    "values": {
                        "home": 9,
                        "away": 2
                    }
                },
                {
                    "heading": "Shots from inside the box",
                    "percentages": false,
                    "values": {
                        "home": 16,
                        "away": 8
                    }
                },
                {
                    "heading": "% Shot Accuracy (excluding blocked shots)",
                    "percentages": false,
                    "values": {
                        "home": 28.6,
                        "away": 0
                    }
                }
            ],
            "key": {
                "homeTeam": "Norwich City",
                "awayTeam": "Hull City"
            }
        },
        "generalPlay": {
            "content": [
                {
                    "heading": "% Possession",
                    "percentages": false,
                    "values": {
                        "home": 56.2,
                        "away": 43.8
                    }
                },
                {
                    "heading": "% Duels Won",
                    "percentages": false,
                    "values": {
                        "home": 51.9,
                        "away": 48.1
                    }
                },
                {
                    "heading": "% Aerial duels won",
                    "percentages": false,
                    "values": {
                        "home": 60,
                        "away": 40
                    }
                },
                {
                    "heading": "Interceptions",
                    "percentages": false,
                    "values": {
                        "home": 7,
                        "away": 23
                    }
                },
                {
                    "heading": "Offsides",
                    "percentages": false,
                    "values": {
                        "home": 1,
                        "away": 1
                    }
                },
                {
                    "heading": "Corners",
                    "percentages": false,
                    "values": {
                        "home": 10,
                        "away": 8
                    }
                }
            ],
            "key": {
                "homeTeam": "Norwich City",
                "awayTeam": "Hull City"
            }
        },
        "distribution": {
            "content": [
                {
                    "heading": "Total Passes",
                    "percentages": false,
                    "values": {
                        "home": 421,
                        "away": 325
                    }
                },
                {
                    "heading": "% of Passes Long",
                    "percentages": false,
                    "values": {
                        "home": 13.5,
                        "away": 18.5
                    }
                },
                {
                    "heading": "% Passing Accuracy",
                    "percentages": false,
                    "values": {
                        "home": 76.5,
                        "away": 70.2
                    }
                },
                {
                    "heading": "% Passing Accuracy opp. Half",
                    "percentages": false,
                    "values": {
                        "home": 0,
                        "away": 0
                    }
                },
                {
                    "heading": "Total Crosses",
                    "percentages": false,
                    "values": {
                        "home": 40,
                        "away": 27
                    }
                },
                {
                    "heading": "% Successful Crosses",
                    "percentages": false,
                    "values": {
                        "home": 20,
                        "away": 29.6
                    }
                }
            ],
            "key": {
                "homeTeam": "Norwich City",
                "awayTeam": "Hull City"
            }
        },
        "defenceAndDiscipline": {
            "content": [
                {
                    "heading": "Tackles",
                    "percentages": false,
                    "values": {
                        "home": 11,
                        "away": 22
                    }
                },
                {
                    "heading": "% Tackles Won",
                    "percentages": false,
                    "values": {
                        "home": 8,
                        "away": 18
                    }
                },
                {
                    "heading": "Clearances",
                    "percentages": false,
                    "values": {
                        "home": 29,
                        "away": 39
                    }
                },
                {
                    "heading": "Fouls Conceded",
                    "percentages": false,
                    "values": {
                        "home": 8,
                        "away": 16
                    }
                },
                {
                    "heading": "Yellow cards",
                    "percentages": false,
                    "values": {
                        "home": 1,
                        "away": 2
                    }
                },
                {
                    "heading": "Red cards",
                    "percentages": false,
                    "values": {
                        "away": 1,
                        "home": 0
                    }
                }
            ],
            "key": {
                "homeTeam": "Norwich City",
                "awayTeam": "Hull City"
            }
        }
    }
}