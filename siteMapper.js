/**
 * Sitemapper, maps thesite from a JSON file and returns the list of endpoints.
 * 
 * @param fun
 *            function(dir, data)
 * @param map
 *            The json map file of the required format
 * @param dir
 *            The directory to start maping this section from
 */
function create(dir, map, fun) {
	var endpoints = [];
	if (dir === "/") {
		dir = "";
	}
	for ( var s in map) {
		if (map.hasOwnProperty(s)) {
			if (s === "%") {
				console.log('assigning \"' + dir + "/\"" + ' a page');
				fun(dir === "" ? '/' : dir, map["%"]);
				endpoints.push([dir === "" ? '/' : dir,map["%"]]);
			} else {
				console.log('creating directory ' + dir + '/' + s);
				endpoints = endpoints.concat(create(dir + '/' + s, map[s], fun));
			}
		}
	}
	return endpoints;
};
exports.create = create;